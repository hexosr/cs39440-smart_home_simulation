#!/usr/bin/env python 

from math import pi 

from sensor import Sensor

import rospy

from std_msgs.msg import Float32


temperature = [20,22,23,25,30,40,35,33,30,26]
count = 0

if __name__ == '__main__':
    
    sensor = Sensor()

    rospy.init_node('temperature_sensor')
    pub = rospy.Publisher('temperature', Float32, queue_size=10)
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        if count == 10:
            count = 0
        sensor.set_value(temperature[count])
        hum = sensor.get_value()

        pub.publish(hum)
        count = count + 1
        rate.sleep()