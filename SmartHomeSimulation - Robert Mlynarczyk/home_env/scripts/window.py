#!/usr/bin/env python

from PySide2.QtWidgets import QApplication,QWidget,QPushButton,QMessageBox,QDesktopWidget
from PySide2.QtGui import QIcon
import sys
import time
import rospy
from std_msgs.msg import Int32
from topic_subscriber import NumberCounter

class Window(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("PySide2 Simple Application")
        self.setGeometry(300,300,300,300)

        self.setMinimumHeight(100)
        self.setMaximumHeight(200)
        self.setMinimumWidth(250)
        self.setMaximumWidth(500)

        self.setIcon()
        self.setButton()
        self.pushButton()
        self.center()
    
    def setIcon(self):
        appIcon = QIcon("icon.png")
        self.setWindowIcon(appIcon)
    
    def setButton(self):
        x = nc.get_counter()
        y = str(x)
        btn1 = QPushButton(y, self)
        btn1.move (50,100)
        
        btn1.clicked.connect(self.quitApp)

    def quitApp(self):
        userInfo = QMessageBox.question(self, "Confirmation" , "Do You Want To Quit The Application?", QMessageBox.Yes | QMessageBox.No)
        if userInfo == QMessageBox.Yes:
            myApp.quit()
        elif userInfo == QMessageBox.No:
            pass

    def center(self):
        qRect = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qRect.moveCenter(centerPoint)
        self.move(qRect.topLeft())

    def pushButton(self):
        self.aboutButton = QPushButton("Open About Box" ,self)
        self.move(100,50)
        self.aboutButton.clicked.connect(self.aboutBox)

    def aboutBox(self):
        QMessageBox.about(self.aboutButton, "About PySide2", "PySide2 is a Cross Platform GUI Library For Python Programming Language")


if __name__ == '__main__':
    nc = NumberCounter()
    nc.get_counter()
    myApp = QApplication(sys.argv)
    window = Window()
    window.show()

    # time.sleep(5)
    # window.resize(600,400)

    myApp.exec_()
    sys.exit(0)
