#!/usr/bin/env python 

from math import pi 

from sensor import Sensor

import rospy

from std_msgs.msg import Float32


humidity = [100,120,100,130,140,150,160,170,150,100]
count = 0

if __name__ == '__main__':
    
    sensor = Sensor()

    rospy.init_node('humidity_sensor')
    pub = rospy.Publisher('humidity', Float32, queue_size=10)
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        if count == 10:
            count = 0
        sensor.set_value(humidity[count])
        hum = sensor.get_value()

        pub.publish(hum)
        count = count + 1
        rate.sleep()