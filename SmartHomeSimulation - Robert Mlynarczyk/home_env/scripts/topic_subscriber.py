#!/usr/bin/env python 

import rospy
from std_msgs.msg import Int32

class NumberCounter:
    def __init__(self):
        self.counter = 0
        self.number_subscriber = rospy.Subscriber("/counter", Int32, self.callback_number)
    
    def callback_number(self, msg):
        self.counter = msg.data
        self.asd = 'IAmUsedInTheOtherCallback'
    
    def get_counter(self):
        return (self.counter) 

if __name__ == '__main__':
    rospy.init_node('number_counter')
    num_counter = NumberCounter()

    print (num_counter.counter)
    rospy.spin()