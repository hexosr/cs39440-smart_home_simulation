#!/usr/bin/env python

class Sensor():
    def __init__(self):
        self.value = 0
        pass

    def get_value(self):
        return self.value

    def set_value(self,v):
        self.value = v
